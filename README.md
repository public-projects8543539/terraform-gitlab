# Terraform-Gitlab

Infrastructura como código con Terraform y GitLab

## Getting started

Revisamos dos formas de trabajo con el uso de Terraform y GitLab

- [ ] GitLab como backend
- [ ] Crear artecfacto en GitLab para guardar estado de Terraform 

## GitLab como backend

Ocupamos el tamplate proporcionado por GitLab y su configuración sugerida para el funcionamiento [documentación](https://docs.gitlab.com/ee/user/infrastructure/iac/)

El ejemplo con el código se encuentra en la rama [main](https://gitlab.com/public-projects8543539/terraform-gitlab/-/tree/main?ref_type=heads)

## Crear artefacto en GitLab para guardar estado de Terraform 

Creamos un artefacto y guardamos el estado de Terraform con configuración en el job del pipeline [documentación](https://docs.gitlab.com/ee/ci/yaml/#artifacts)

El ejemplo con el código se encuentra en la rama [artifact](https://gitlab.com/public-projects8543539/terraform-gitlab/-/tree/artifact?ref_type=heads)

## Contributing
¡Las contribuciones son bienvenidas! Si deseas mejorar o agregar nuevas funcionalidades, sigue estos pasos:

- Haz un fork de este repositorio.
- Realiza tus cambios en tu fork.
- Envía un pull request a este repositorio.

## Authors and acknowledgment
Este proyecto fue creado y es mantenido por Aquiles Lázaro.

- Nombre: Aquiles Lázaro
- Correo Electrónico: aquileslazaroh@gmail.com
- Perfil de GitLab: [GitLab](https://gitlab.com/aquileslh)
- Perfil de GitHub: [GitHub](https://github.com/aquileslh)

## License
Este proyecto está bajo la [Licencia MIT](LICENSE).

La Licencia MIT es una licencia de código abierto muy permisiva que permite a otros utilizar, modificar y redistribuir tu código, ya sea en proyectos comerciales o no comerciales, siempre que se incluya el aviso de copyright y la licencia original.

Puedes encontrar más detalles en el archivo [LICENSE](LICENSE) o en la [página de la Licencia MIT](https://opensource.org/licenses/MIT).
