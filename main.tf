# Configuración de Terraform
terraform {
  # Se especifica el proveedor requerido (AWS) y su fuente (hashicorp/aws)
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  # Versión mínima requerida de Terraform
  required_version = ">= 1.2.0"
}

# Configuración del proveedor AWS
provider "aws" {
  # Región de AWS a la que se va a conectar
  region = "us-east-1"
}

# Generar una cadena aleatoria para usar en el nombre del bucket S3
resource "random_string" "bucket_name" {
  length  = 6     # Longitud de la cadena aleatoria
  special = false # No incluir caracteres especiales en la cadena
  upper   = false
}

# Recurso para crear un bucket S3
resource "aws_s3_bucket" "mi_bucket" {
  # Nombre único para el bucket S3
  bucket = "terraform-gitlab-${random_string.bucket_name.result}"

  # Etiquetas para el bucket S3
  tags = {
    Name        = "test-terraform-gitlab"
    Environment = "Dev"
  }
}